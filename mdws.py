#!/usr/bin/env python
from JAK.Application import JWebApp

def manjaro_web():
    # v1.0.0
    start = "https://gitlab.manjaro.org"
    icon = "/usr/share/icons/manjaro/maia/maia.svg"

    toolbar = {"name": "Gitlab", "url": start},\
              {"name": "Chat", "url": "https://mattermost.manjaro.org"},\
              {"name": "Video", "url": "https://meet.manjaro.org"},\
              {"name": "Mail", "url": "https://webmail.manjaro.org"},\
              {"name": "Forum", "url": "https://forum.manjaro.org"},\
              {"name": "Wiki", "url": "https://wiki.manjaro.org"}

    webapp = JWebApp(title="Manjaro Dev Web Services", icon=icon, web_contents=start, online=True,
                     toolbar=toolbar)

    return webapp


manjaro_web().run()